import React, { Component } from "react";
import "./App.css";

import WebcamCapture from "./Webcam";

class App extends Component {
  submit = async (image, message) => {
    // api request
    const apiResp = await fetch("/upload", {
      body: JSON.stringify(image),
      method: "POST"
    });
    console.log(apiResp);
  };

  render() {
    return (
      <div className="App">
        <WebcamCapture submit={this.submit} />
      </div>
    );
  }
}

export default App;
